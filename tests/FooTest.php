<?php
declare(strict_types=1);

namespace Test;

use App\Foo;
use PHPUnit\Framework\TestCase;

class FooTest extends TestCase
{
    /**
     * @covers \App\Foo::getState
     */
    public function testFoo(): void
    {
        $foo = new Foo(5);
        self::assertSame(5, $foo->getState());
    }
}
